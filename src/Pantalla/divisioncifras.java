/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pantalla;

import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.math.RoundingMode;
import javax.swing.JOptionPane;

/**
 *
 * @author rafa_
 */
public class divisioncifras extends javax.swing.JFrame {

    private void cerrar(){
        String botones[] = {"Cerrar","Cancelar"};
        int eleccion = JOptionPane.showOptionDialog(this,"¿Desea cerrar la aplicacion?","",0,0, null, botones, this);
        if(eleccion == JOptionPane.YES_OPTION){
            System.exit(0);
        }
    }
    public divisioncifras() {
        initComponents();
        this.setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jTextField3 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jLabel1.setText("Ingrese los datos");

        jTextField2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField2KeyReleased(evt);
            }
        });

        jButton1.setText("Generar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel2.setText("Resultado de operación");

        jLabel3.setText("Cifras significativas");

        jButton2.setText("Regresar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel4.setText("/");

        jLabel5.setText("Dividendo");

        jLabel6.setText("Divisor");

        jMenu1.setText("Archivo");

        jMenuItem1.setText("Regresar");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Cerrar");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Ayuda");

        jMenuItem3.setText("Instrucciones");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(89, 89, 89)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextField2)
                    .addComponent(jTextField1)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(60, 60, 60)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(112, 112, 112)
                        .addComponent(jButton2)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(61, 61, 61)
                        .addComponent(jLabel4)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addContainerGap(22, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        cerrar();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        Cifras obj = new Cifras();
        obj.setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        cerrar();
    }//GEN-LAST:event_formWindowClosing

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        Cifras obj = new Cifras();
        obj.setVisible(true);
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed
        
    public int retcifras (String numero) {
        char[] caracteres = numero.toCharArray();
                boolean punto=false;  
                int cantcaracteres = 0;
                int cifras;
                for(int i=0;i<caracteres.length;i++){
                    if(caracteres[i]=='.'){
                        punto=true;  
                    }   
                    cantcaracteres++;
                }
                boolean punto2 = false;
                if(punto==true){ 
                    int n = 0; 
                    int i = 0;
                    cifras = cantcaracteres;
                    while((caracteres[i]=='0' || caracteres[i]=='.')&& i<caracteres.length) {
                        n++;
                        i++;
                        if(caracteres[0]=='.' || caracteres[i]=='.') {
                            punto2=true;
                        }
                    }
                    cifras=cifras-n;
                    if(punto2==false)
                        cifras--;
                }
                else {
                    int n = 0; 
                    int i = 0;
                    cifras = cantcaracteres;
                    while(caracteres[i]=='0' && i<caracteres.length) {
                        n++;
                        i++;
                    }
                    cifras=cifras-n;
                }
        return cifras;
    }
    
    public int ent (String numero) {
        char[] caracteres = numero.toCharArray();
        boolean punto=false;  
        int cantidad = 0;
        for(int i=0;i<caracteres.length;i++){
            if(caracteres[i]=='.'){
                punto=true;  
            }   
        }
        if(punto==true) {
            int i = 0;
            while(caracteres[i]!='.') {
                cantidad++;
                i++;
            }
        }
        else {
            cantidad = caracteres.length;
        }
        return cantidad;
        
    }
    
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
         if(!this.jTextField1.getText().isEmpty() && !this.jTextField2.getText().isEmpty()) {
        String array[] = new String[2];
        array[0] = this.jTextField1.getText();
        array[1] = this.jTextField2.getText();
            double div = 1;
            int menorcs=0;
            int espacios = 0;
            for(int i=0;i<array.length;i++){
                double dnum = Double.parseDouble(array[i]);
                if(i==0) {
                    div=dnum;
                    menorcs=this.retcifras(array[i]);
                }
                else {
                    div=div/dnum;
                    if(menorcs>this.retcifras(array[i])) {
                        menorcs=this.retcifras(array[i]);
                    }
                }
            }      
            String d = String.valueOf(div);  
            double dob = Double.parseDouble(d);
            char[] caracteres = d.toCharArray();
            int ult = caracteres.length-1;
            int quitar = this.retcifras(d) - menorcs;
            if(dob<0.5) {
                char auxi;
                int i = 2;
                while(caracteres[i]=='0' && i<caracteres.length) {
                    espacios++;
                    i++;
                }
                BigDecimal bd = new BigDecimal(d);
                bd = bd.setScale(menorcs, RoundingMode.HALF_UP);
                String nc = String.valueOf(bd.doubleValue());
                char[] caract = nc.toCharArray();
                espacios=espacios+menorcs;
                for(int a=1;a<=espacios;a++) {
                    auxi = caract [a];
                    caract [a] = caract [a+1];
                    caract [a+1] = auxi;
                }
                int esp = 0;
                while(caract[esp]=='0') {
                    esp++;
                }
                char [] nuevoscarac = new char[menorcs];
                    int o = esp;
                    int u = 0;  
                    while(caract[o]!='.') {
                        nuevoscarac[u] = caract[o];
                        o++;
                        u++;
                    }
                String numeros = String.valueOf(nuevoscarac);
                if(espacios == 1) {
                    this.jTextField3.setText("0.00" + numeros + " x10^2");
                    this.jTextField4.setText("1");
                }
                else {
                    this.jTextField3.setText(numeros + " x10^-" + espacios);
                    String n = String.valueOf(this.retcifras(numeros));
                    this.jTextField4.setText(n);
                }
            }
            else {
                if(this.ent(d)<=menorcs) {
                    int decimales = (this.retcifras(d) - this.ent(d)) - quitar;
                    BigDecimal bd = new BigDecimal(d);
                    bd = bd.setScale(decimales, RoundingMode.HALF_UP);
                    if(decimales==0) {
                        int numero = (int)bd.doubleValue();
                        String no = String.valueOf(numero);
                        this.jTextField3.setText(no);
                        String n = String.valueOf(this.retcifras(no));
                        this.jTextField4.setText(n);
                    }
                    else {
                        String no = String.valueOf(bd.doubleValue());
                        this.jTextField3.setText(no);
                        String n = String.valueOf(this.retcifras(no));
                        this.jTextField4.setText(n);
                    }
                }
                if(this.ent(d)>menorcs) {
                    char auxi;
                    int i = 0;
                    while(caracteres[i]!='.' && i<caracteres.length) {
                        espacios++;
                        i++;
                    }
                    for(int a=espacios;a>0;a--) {
                        auxi = caracteres [a];
                        caracteres [a] = caracteres [a-1];
                        caracteres [a-1] = auxi;
                    }
                    String numero = String.valueOf(caracteres);
                    ult = menorcs+1;
                     char [] nuevoscarac = new char[ult];
                    for(int c=0;c<nuevoscarac.length;c++) {
                        nuevoscarac[c]=caracteres[c];
                    }
                String numeros = String.valueOf(nuevoscarac);
                this.jTextField3.setText(numeros + " x10^" + espacios);
                String n = String.valueOf(this.retcifras(numeros));
                        this.jTextField4.setText(n);
                }
            }
         } else {
            this.jTextField3.setText("");
            this.jTextField4.setText("");
         }
    }//GEN-LAST:event_jButton1ActionPerformed
    
    private void jTextField2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField2KeyReleased
        // TODO add your handling code here:
        if(evt.getKeyCode()==KeyEvent.VK_ENTER && (!this.jTextField2.getText().isEmpty() && !this.jTextField2.getText().isEmpty())){
            String array[] = new String[2];
            array[0] = this.jTextField1.getText();
            array[1] = this.jTextField2.getText();
            String numeros = "";
            double div = 1;
            int menorcs=0;
            int espacios = 0;
            for(int i=0;i<array.length;i++){
                double dnum = Double.parseDouble(array[i]);
                if(i==0) {
                    div=dnum;
                    menorcs=this.retcifras(array[i]);
                }
                else {
                    div=div/dnum;
                    if(menorcs>this.retcifras(array[i])) {
                        menorcs=this.retcifras(array[i]);
                    }
                }
            }
            String d = String.valueOf(div); 
            double dob = Double.parseDouble(d);
            char[] caracteres = d.toCharArray();
            int ult = caracteres.length-1;
            int quitar = this.retcifras(d) - menorcs;
            if(dob<0.5) {
                    char auxi;
                BigDecimal bd = new BigDecimal(d);
                bd = bd.setScale(10, RoundingMode.HALF_UP);
                String bd1 = String.valueOf(bd);
                char[] carac = bd1.toCharArray(); 
                int i = 2;
                while(carac[i]=='0') {
                    espacios++;
                    i++;
                }
                espacios=espacios+menorcs;
                for(int a=1;a<=espacios;a++) {
                    auxi = carac [a];
                    carac [a] = carac [a+1];
                    carac [a+1] = auxi;
                }
                int esp = 0;
                while(carac[esp]=='0') {
                    esp++;
                }
                char [] nuevoscarac = new char[menorcs];
                    int o = esp;
                    int u = 0;  
                    while(u<menorcs) {
                        nuevoscarac[u] = carac[o];
                        o++;
                        u++;
                    }
                numeros = String.valueOf(nuevoscarac);
                    if(espacios == 1) {
                        this.jTextField3.setText("0.00" + numeros + " x10^2");
                        this.jTextField4.setText("1");
                    }
                    else {
                        this.jTextField3.setText(numeros + " x10^-" + espacios);
                        numeros = String.valueOf(this.retcifras(numeros));
                    }
                }
            else {
                if(this.ent(d)<=menorcs) {
                    int decimales = (this.retcifras(d) - this.ent(d)) - quitar;
                    BigDecimal bd = new BigDecimal(d);
                    bd = bd.setScale(decimales, RoundingMode.HALF_UP);
                    if(decimales==0) {
                        int numero = (int)bd.doubleValue();
                        numeros = String.valueOf(numero);
                        this.jTextField3.setText(numeros);
                    }
                    else {
                        numeros = String.valueOf(bd.doubleValue());
                        this.jTextField3.setText(numeros);
                    }
                }
                if(this.ent(d)>menorcs) {
                    char auxi;
                    int i = 0;
                    while(caracteres[i]!='.' && i<caracteres.length) {
                        espacios++;
                        i++;
                    }
                    for(int a=espacios;a>0;a--) {
                        auxi = caracteres [a];
                        caracteres [a] = caracteres [a-1];
                        caracteres [a-1] = auxi;
                    }
                    ult = menorcs+1;
                     char [] nuevoscarac = new char[ult];
                    for(int c=0;c<nuevoscarac.length;c++) {
                        nuevoscarac[c]=caracteres[c];
                    }
                numeros = String.valueOf(nuevoscarac);
                this.jTextField3.setText(numeros + " x10^" + espacios);
                }
            }
            if(this.retcifras(d)<menorcs) {
                int agregar = menorcs-this.retcifras(numeros);
                String ceros = "";
                for(int i=0;i<agregar;i++) {
                    ceros += "0";
                }
                this.jTextField3.setText(numeros + ceros);
            }
            String tx4 = String.valueOf(menorcs);
            this.jTextField4.setText(tx4);
        }
         if(evt.getKeyCode()==KeyEvent.VK_ENTER && (this.jTextField2.getText().isEmpty() || this.jTextField2.getText().isEmpty())){
            this.jTextField3.setText("");
        this.jTextField4.setText("");
        }
    }//GEN-LAST:event_jTextField2KeyReleased

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        JOptionPane.showMessageDialog(null,"En esta ventana podrás realizar\n" +
"las divisiones.\n" +
"Necesitarás ingresar el dividendo\n" +
"en la primera casilla libre, así\n" +
"como el divisor en la segunda.\n" +
"Luego es necesario oprimir el\n" +
"botón \"Generar\" para observar\n" +
"los resultados en la parte \n" +
"inferior.\n" +
"\n" +
"El botón \"Regresar\" te ayudará a\n" +
"regresar al menú.");
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(divisioncifras.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(divisioncifras.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(divisioncifras.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(divisioncifras.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new divisioncifras().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    // End of variables declaration//GEN-END:variables
}
